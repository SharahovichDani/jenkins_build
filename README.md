# Automation Build NPM Application

## Introduction

This is an automated declarative Jenkins pipeline that builds and increments versions. The project is based on Jenkins, Shared Library, NPM, and Docker.

As a result of building the pipeline, the application will be connected to the shared library and updated, the app will be built, the app will be pushed to the Docker hub, and the repository will be updated in Git.

Shared Library: https://gitlab.com/SharahovichDani/jenkins_shared_library

## Setup

1. Clone the repo with ```git clone https://gitlab.com/SharahovichDani/jenkins_build.git``` 

2. Use your Jenkins application and install the Credential Binding plugin. 

3. Create 2 Credentials in Jenkins
```
Docker hub account:
Type: Username with password
ID: docker-hub
Username: Your username to Docker hub
Password: Your Password to Docker hub

Github account:
Type: Secret text
ID: github-credentials
Secret: Create Personal access token (classic) >>  https://github.com/settings/tokens

Optional: If your Repository in Gitlab
Type: Username with password
ID: gitlab-credentials
Username: Your username to Gitlab
Password: Your Password to Gitlab
```
4. Install Nodejs tool in Jenkins named 'node'.

5. Create a pipeline in Jenkins and choose your repository at Source Code Management.

6. When you build your pipeline, it will ask for parameters.
```
Version Choise: Major, Minor, Patch
git Choise: Github, Gitlab
mail: Write your mail for git config
Repository: Your repository name in Docker hub
```


## Tools Requirements

* Jenkins (Docker need be installed)

* NodeJs








